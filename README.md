# gitea-generic-package

Drone plugin to upload generic packages to Gitea.

## Setup

To use this image, your Drone instance should be able to `pull` from `registry.gitlab.com`.

Depending on your deployment, you need to run:

```shell
docker login registry.gitlab.com
```

With a containerized deployment, you have to either share the file `~/.config/docker` with Drone or define secrets containing the content of that file as explained [here](https://docs.drone.io/pipeline/docker/syntax/images/#pulling-private-images).

## Usage

With basic HTTP authentication:

```yaml
  - name: upload package
    image: registry.gitlab.com/drone-plugins/gitea-generic-package
    settings:
      gitea_hostname: gitea.domain.xyz
      gitea_username:
        from_secret: gitea_username
      gitea_password:
        from_secret: gitea_password
      package_owner: ${DRONE_REPO_OWNER}
      package_name: ${DRONE_REPO_NAME}
      package_version: ${DRONE_COMMIT:0:8}
      package_path: path/to/package.art
```

With token authentication:

```yaml
  - name: upload package
    image: registry.gitlab.com/drone-plugins/gitea-generic-package
    settings:
      gitea_hostname: gitea.domain.xyz
      gitea_token:
        from_secret: gitea_token
      package_owner: ${DRONE_REPO_OWNER}
      package_name: ${DRONE_REPO_NAME}
      package_version: ${DRONE_COMMIT:0:8}
      package_path: path/to/package.art
```

> Note: The API requires a token with at least `read-write` permissions to the `packages` endpoints.

Disabling TLS certs:

```yaml
  - name: upload package
    image: registry.gitlab.com/drone-plugins/gitea-generic-package
    settings:
      no_tls: True
      gitea_hostname: gitea.domain.xyz
      gitea_token:
        from_secret: gitea_token
      package_owner: ${DRONE_REPO_OWNER}
      package_name: ${DRONE_REPO_NAME}
      package_version: ${DRONE_COMMIT:0:8}
      package_path: path/to/package.art
```

Using the CLI directly:

```yaml
  - name: upload package
    image: registry.gitlab.com/drone-plugins/gitea-generic-package
    commands:
    - |
      python /home/bot/main.py \
      -u gitea_username -p gitea_password \
      gitea.domain.xyz ${DRONE_REPO_OWNER} ${DRONE_REPO_NAME} ${DRONE_COMMIT:0:8} path/to/package.art
```

> Note: You can run `python /home/bot/main.py --help` for instructions on how to use the script.

## Contributing

Contributions are very welcome!

1. [Fork](https://docs.gitlab.com/ee/user/project/repository/forking_workflow.html) this repository
2. [Create a new branch](https://docs.gitlab.com/ee/user/project/repository/branches/#create-branch) in your forked repository called `my-new-feature`
3. Make changes to the code in the new branch
4. [Create a merge request](https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html#when-you-work-in-a-fork) to include your changes to this repo.

## License

GPLv3
