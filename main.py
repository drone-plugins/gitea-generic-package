import click
import requests

from pathlib import Path

@click.command()
@click.argument("hostname", envvar="PLUGIN_GITEA_HOSTNAME", type=click.STRING)
@click.argument("owner", envvar="PLUGIN_PACKAGE_OWNER", type=click.STRING)
@click.argument("package_name", envvar="PLUGIN_PACKAGE_NAME", type=click.STRING)
@click.argument("package_version", envvar="PLUGIN_PACKAGE_VERSION", type=click.STRING)
@click.argument("package_path", envvar="PLUGIN_PACKAGE_PATH", type=click.Path(exists=True, file_okay=True, dir_okay=False, readable=True, path_type=Path))
@click.option("--no-tls", envvar="PLUGIN_NO_TLS", is_flag=True, default=False)
@click.option("-u", "--username", envvar="PLUGIN_GITEA_USERNAME", type=click.STRING)
@click.option("-p", "--password", envvar="PLUGIN_GITEA_PASSWORD", type=click.STRING)
@click.option("-t", "--token", envvar="PLUGIN_GITEA_TOKEN", type=click.STRING)
def cli(hostname, owner, package_name, package_version, package_path, no_tls, username, password, token):
    url = f"http{'' if no_tls else 's'}://{hostname}/api/packages/{owner}/generic/{package_name}/{package_version}/{package_path.name}"
    files = {package_path.name: open(package_path, "rb")}

    click.echo(f"Uploading {package_path.name} to {owner}/generic/{package_name}/{package_version}/{package_path.name}")

    if token is not None:
        click.echo("Using token authentication.")

        headers = {
            "accept": "application/json",
            "Authorization": f"token {token}",
            "Content-Type": "application/json"
        }

        r = requests.put(url, headers=headers, files=files)
    elif username is not None and password is not None:
        click.echo("Using basic http authentication.")
        r = requests.put(url, auth=(username, password), files=files)
    else:
        click.echo("No credentials supplied.")
        r = requests.put(url, files=files)

    r.raise_for_status()

    click.echo("The package has been published.")

if __name__ == "__main__":
    cli()
